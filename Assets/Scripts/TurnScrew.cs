﻿using UnityEngine;
using System.Collections;

public class TurnScrew : MonoBehaviour {

	public bool ScrewActivated = false;
	public bool ScrewShow = false;

	private Animator ThisAnimator = null;

	// Use this for initialization
	void Awake () 
	{
		ThisAnimator = GetComponent<Animator> ();
	}

	public void ActivateScrew(bool ScrewActivate)
	{
		//Should sphere be activated?
		ScrewActivated = ScrewActivate;
	}

	public void ShowScrewObject(bool HideShow)
	{
		if (!ScrewActivated)
			return;

		ScrewShow = HideShow;
		ThisAnimator.SetBool ("ShowScrew", ScrewShow);
	}
}
