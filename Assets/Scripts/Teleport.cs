﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour 
{
	public Transform Destination = null;
	public Vector3 DestRot = Vector3.zero;

	void OnTriggerEnter(Collider Other)
	{
		if (!Other.CompareTag ("Player"))
			return;

		Transform PlayerTransform = Other.GetComponent<Transform> ();
		PlayerTransform.position = Destination.position;
		PlayerTransform.rotation = Quaternion.Euler (DestRot);
	}
}
