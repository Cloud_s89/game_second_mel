﻿using UnityEngine;
using System.Collections;
//-----------------------------------------------------------------
public class Extruder : MonoBehaviour 
{
	//List of objects
	public Transform[] ExtrudeObjects;

	//Reference to transform
	private Transform ThisTransform = null;

	//Speed of extrusion
	public float Speed = 1f;

	//Amount of extrusion
	public float ExtrusionDepth = 1f;

	//Extrude Direction
	private Vector3 ExtrudeDirection = Vector3.zero;

	//Should we move?
	public bool ShouldExtrude = false;
	//-----------------------------------------------------------------
	// Use this for initialization
	void Awake () 
	{
		ThisTransform = GetComponent<Transform> ();
	}
	//-----------------------------------------------------------------
	public IEnumerator Extrude()
	{
		float DistanceExtrude = 0f;

		//While we should extrude
		while (ShouldExtrude) 
		{
			//Cycle through all objects
			foreach (Transform T in ExtrudeObjects) 
			{
				//Extrude
				Vector3 ExtrudeAmount = T.up * Speed * Time.deltaTime;
				DistanceExtrude += ExtrudeAmount.magnitude;
				T.position += ExtrudeAmount;

				//Should we stop extruding this object?
				if (DistanceExtrude >= ExtrusionDepth) 
				{
					T.position += ExtrudeAmount;
					ShouldExtrude = false;
					break;
				}
			}

			//Wait for next frame
			yield return null;
		}

		//Round objects to nearest int
		foreach (Transform T in ExtrudeObjects)
		{
			T.position = new Vector3(Mathf.RoundToInt(T.position.x),
									Mathf.RoundToInt(T.position.y), 
									Mathf.RoundToInt(T.position.z));
		}
	}
	//-----------------------------------------------------------------
	public void StartExtrude()
	{
		ShouldExtrude = true;
		StartCoroutine(Extrude());
	}
	//-----------------------------------------------------------------
}
