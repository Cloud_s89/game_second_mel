﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
	public static int HighestReachedLevel = 0;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
		HighestReachedLevel = PlayerPrefs.GetInt ("HighestReachedLevel",0);
	
	}

	public static void SaveLevel(int LevelNumber)
	{
		HighestReachedLevel = LevelNumber;
		PlayerPrefs.SetInt ("HighestReachedLevel", LevelNumber);
		PlayerPrefs.Save ();
	}
}
