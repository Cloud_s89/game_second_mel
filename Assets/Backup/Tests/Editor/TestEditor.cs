﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Test))]
public class TestEditor : Editor
{
    private bool _message;

    public override void OnInspectorGUI()
    {
        if (!_message)
            _message = GUILayout.Button("Click for custom level editor");

        if (_message)
            EditorGUILayout.LabelField("NO CLAUDIO!");
        
    }
}
