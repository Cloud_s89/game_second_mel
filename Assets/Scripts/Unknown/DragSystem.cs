﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragSystem : MonoBehaviour,IBeginDragHandler, IDragHandler,IEndDragHandler 
 {

	public void OnBeginDrag(PointerEventData eventData){
		
	}


	public void OnDrag(PointerEventData eventData)
	{
		Debug.Log ("OnDrag");

		Plane plane = new Plane(Vector3.up, transform.position);
		Ray ray = eventData.pressEventCamera.ScreenPointToRay(eventData.position);
		float distamce;
		if (plane.Raycast(ray, out distamce))
		{
			transform.position = ray.origin + ray.direction * distamce;
		}
	
		
			
		}

	public void OnEndDrag(PointerEventData eventData)
	{
	}



}
